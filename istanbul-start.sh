#!/bin/bash
set -u
set -e

mkdir -p qdata/logs
echo "[*] Starting Constellation nodes"
./constellation-start.sh $1 $2 $3

echo "[*] Starting Ethereum nodes"
set -v
ARGS="--nodiscover --syncmode full --mine --rpc --wsaddr 0.0.0.0 --rpcapi admin,db,eth,debug,miner,net,shh,txpool,personal,web3,quorum,istanbul"
PRIVATE_CONFIG=qdata/c2/tm.ipc nohup geth --datadir qdata/dd2 $ARGS --wsport 22001 --port 21001 --rpcaddr 0.0.0.0 --rpc --rpcport 33001 --rpcapi web3,eth,personal,miner,net,txpool --rpccorsdomain="*"  2>>qdata/logs/2.log &

set +v

echo
echo "All nodes configured. See 'qdata/logs' for logs, and run e.g. 'geth attach qdata/dd1/geth.ipc' to attach to the first Geth node."
echo "To test sending a private transaction from Node 1 to Node 7, run './runscript.sh script1.js'"
